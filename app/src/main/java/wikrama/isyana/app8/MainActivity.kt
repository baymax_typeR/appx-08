package wikrama.isyana.app8

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

    val RC_SUKSES : Int = 100
    var bgH : String = "YELLOW"
    var fHead : Int = 30
    var fTitle : Int = 30
    var bg : String = "WHITE"
    var isi : String = "Melanjutkan Avengers Infinity War, dimana kejadian setelah Thanos berhasil mendapatkan semua infinity stones dan memusnahkan 50% semua mahluk hidup di alam semesta. Akankah para Avengers berhasil mengalahkan Thanos?"
    lateinit var pref : SharedPreferences
    val PREF_NAME = "Setting"
    val FIELD_FONT_HEADER = "Font_Size_Header"
    val FIELD_FONT_TITLE = "Font_Size_Title"
    val FIELD_BACK = "BACKGROUND"
    val FIELD_BACKHEAD = "BACKGROUND_HEADER"
    val FIELD_TEXT = "text"
    val DEF_FONT_HEADER = fHead
    val DEF_FONT_TITLE = fTitle
    val DEF_BACK = bg
    val DEF_BACKHEAD = bgH
    val DEF_ISI = isi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txtIsi.setText(pref.getString(FIELD_TEXT,DEF_ISI))
        fHead = pref.getInt(FIELD_FONT_HEADER,DEF_FONT_HEADER)
        fTitle = pref.getInt(FIELD_FONT_TITLE,DEF_FONT_TITLE)
        bg = pref.getString(FIELD_BACK,DEF_BACK).toString()
        bgH = pref.getString(FIELD_BACKHEAD,DEF_BACKHEAD).toString()

        Font_Header()
        Font_Title()
        background()
        backgroundHeader()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSet->{
                var intent = Intent(this,SettingActivity::class.java)
                intent.putExtra("x",txtIsi.text.toString())
                startActivityForResult(intent,RC_SUKSES)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == RC_SUKSES)
                txtIsi.setText(data?.extras?.getString("isidetail"))
            bg = data?.extras?.getString("bgHcolor").toString()
            bgH = data?.extras?.getString("bgcolor").toString()
            fHead = data?.extras?.getInt("tHead").toString().toInt()
            fTitle = data?.extras?.getInt("tTitle").toString().toInt()
            pref = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
            val prefEdit = pref.edit()
            prefEdit.putInt(FIELD_FONT_HEADER,fHead)
            prefEdit.putInt(FIELD_FONT_TITLE,fTitle)
            prefEdit.putString(FIELD_TEXT,isi)
            prefEdit.putString(FIELD_BACK,bg)
            prefEdit.putString(FIELD_BACKHEAD,bgH )
            prefEdit.commit()

            Font_Header()
            Font_Title()
            background()
            backgroundHeader()
        }
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }
    fun background(){
        if(bgH=="Blue"){
            txtHeader.setBackgroundColor(Color.BLUE)
        }
        else if(bgH=="Yellow"){
            txtHeader.setBackgroundColor(Color.YELLOW)
        }
        else if(bgH=="Green"){
            txtHeader.setBackgroundColor(Color.GREEN)
        }
        else if(bgH=="Black"){
            txtHeader.setBackgroundColor(Color.BLACK)
        }
        else if(bgH=="White"){
            txtHeader.setBackgroundColor(Color.WHITE)
        }
        else{
            txtHeader.setBackgroundColor(Color.RED)
        }
    }

    fun backgroundHeader(){
        if(bg=="BLUE"){
            constraintLayout.setBackgroundColor(Color.BLUE)
        }
        else if(bg=="YELLOW"){
            constraintLayout.setBackgroundColor(Color.YELLOW)
        }
        else if(bg=="GREEN"){
            constraintLayout.setBackgroundColor(Color.GREEN)
        }
        else if(bg=="BLACK"){
            constraintLayout.setBackgroundColor(Color.BLACK)
        }
    }

    fun Font_Header(){
        txtHeader.textSize = fHead.toFloat()
    }

    fun Font_Title(){
        txtTitle.textSize = fTitle.toFloat()
    }
}
