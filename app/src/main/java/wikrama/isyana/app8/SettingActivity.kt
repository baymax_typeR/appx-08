package wikrama.isyana.app8

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener {

    val arrayWarna = arrayOf("Blue","Yellow","Green","Black","White")
    lateinit var adapterSpin : ArrayAdapter<String>
    var bgH : String = ""
    var fHeader : Int = 0
    var fTitle : Int = 0
    var x : String = ""
    var bg : String = ""


    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            fHeader = progress.toFloat().toInt()
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }

    }
    val onSeek2 = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            fTitle = progress.toFloat().toInt()
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayWarna)
        spWarnaBg.adapter = adapterSpin
        var paket: Bundle? = intent.extras
        edDetail.setText(paket?.getString("x"))
        skHeader.setOnSeekBarChangeListener(onSeek)
        skTitle.setOnSeekBarChangeListener(onSeek2)
        radioGroup2.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.radBlue -> bgH = "BLUE"
                R.id.radYellow -> bgH = "YELLOW"
                R.id.radGreen -> bgH = "GREEN"
                R.id.radBlack -> bgH = "BLACK"
            }
            btnSave.setOnClickListener(this)
        }
        spWarnaBg.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext, "Tidak ada yang dipilih", Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Toast.makeText(baseContext, adapterSpin.getItem(position), Toast.LENGTH_SHORT)
                    .show()
                bg = adapterSpin.getItem(spWarnaBg.selectedItemPosition).toString()
            }
        }
    }
    override fun onClick(v: View?) {
        x = edDetail.text.toString()
        Toast.makeText(this, "Perubahan Telah Disimpan", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun finish() {
        var intent = Intent()
        intent.putExtra("bgcolor",bg)
        intent.putExtra("bgHcolor",bgH)
        intent.putExtra("tHead",fHeader)
        intent.putExtra("tTitle",fTitle)
        intent.putExtra("isidetail",x)
        setResult(Activity.RESULT_OK,intent)
        super.finish()
    }
}